from flask import Flask, session, redirect
from .views.auth import auth
from .views.feed import feed
from .views.profile import profile
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__, template_folder='templates')
app.secret_key = os.environ.get('secret.key')
app.register_blueprint(auth)
app.register_blueprint(feed)
app.register_blueprint(profile)


@app.route('/', methods=['GET'])
def index():
    if session.get('username')!=None:
        return redirect('/feed')
    else:
        return redirect('/auth')


@app.errorhandler(404)
def handle_404(e):
    return('<h2>Nothing here</h2><a href="/feed">Click here to go back</a>')


@app.errorhandler(405)
def handle_405(e):
    return('<h2>Wrong request method</h2><a href="/feed">Click here to go back</a>')
