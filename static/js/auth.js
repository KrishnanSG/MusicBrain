const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const loginButton = document.getElementById('login')
const signup = document.getElementById('signup-button')
const container = document.getElementById('container');
const welcome = document.getElementById('welcome')

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});

loginButton.addEventListener('click', () => {
	username = document.getElementById("signInUsername").value
	password = document.getElementById("signInPassword").value

	if (username.length <= 4 || password <= 0) {
		alert("Please entry valid login credentials")
	}
	else {
		fetch('/auth/', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				"username": username,
				"password": password,
				"endpoint": "login"
			})
		})
			.then(response => response.json())
			.then(res => {
				if (res['status']) {
					window.location.replace('https://accounts.spotify.com/authorize?client_id=50212808d83d47eea824f30a089b76b4&response_type=code&redirect_uri=https://musicbrain.herokuapp.com/post-login&scope=user-read-private+user-library-modify+user-library-read+user-top-read+user-read-currently-playing+user-read-recently-played+playlist-modify-private+playlist-read-private+playlist-read-collaborative+playlist-modify-public')
				}
				else {
					alert(res['message'])
				}
			})
			.catch(err => {
				alert("Request failed. Try Again")
			})
	}
})

signup.addEventListener('click', () => {
	username = document.getElementById('signup_username').value
	password = document.getElementById('signup_password').value
	email = document.getElementById('signup_email').value
	confirm = document.getElementById('signup_confirm_password').value
	var slash=username.indexOf("/")
	if (username.length > 20 || username.length <= 4) {
		alert("Choose an username having 5-20 characters")
	}
	else if (email.length <= 0) {
		alert("Invalid email id")
	}
	else {
		if (password.length <= 4 || password.length > 20) {
			alert("Choose a password having 5-20 characters")
		}
		else if (password != confirm) {
			alert("Password mismatch. Re-type confirm password")
		}
		else if (slash != -1){
			alert("Username should not contain /")
		}
		else {
			fetch('/auth/', {
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					"username": username,
					"password": password,
					"email": email,
					"endpoint": "signup"
				})
			})
				.then(response => response.json())
				.then(res => {
					if (res['status']) {
						window.location.replace('/welcome')
					}
					else {
						alert(res['message'])
					}
				})
				.catch(err => {
					alert("Request failed. Try Again")
				}) 

		}
	}
})