from flask import Flask, Blueprint, render_template, session, request, redirect
import requests
import json
import os

auth = Blueprint('auth', __name__, template_folder="templates",
                 static_folder="static")


@auth.route('/auth/', methods=['GET', 'POST'])
def handle_auth():
    if request.method == 'GET':
        return render_template('auth.html')
    elif request.method == 'POST':
        payload = request.get_json()
        url = "https://musicbrain-backend.herokuapp.com/auth/" + \
            payload.get('endpoint')
        response = requests.request("POST", url, json=payload)
        response = json.loads(response.content)

        if response['status'] == 1:
            if payload.get('endpoint') == "login":
                session['username'] = response['username']
                session['token'] = response['token']
                session['playlist_id'] = response['playlist_id']
                session['collab_reco']= response['collab_reco']

                url = "http://musicbrain-backend.herokuapp.com/friends/following?username=" + \
                    session['username']
                friends_response = requests.request("GET", url)
                friends_response = json.loads(friends_response.content)

                url = "http://musicbrain-backend.herokuapp.com/friends/requested?username=" + \
                    session['username']
                requested_response = requests.request("GET", url)
                requested_response = json.loads(requested_response.content)

                session['friends'] = friends_response['friends']
                session['requested'] = requested_response['friends']

            else:
                session['username'] = payload.get('username')
        return(response)


@auth.route('/welcome', methods=['GET', 'POST'])
def welcome():
    if request.method == 'GET':
        return render_template('welcome.html', username=session.get('username')or"User")
    elif request.method == 'POST':
        payload = request.get_json()
        payload['username'] = session['username']
        if payload.get('endpoint') == 'year':
            url = "https://musicbrain-backend.herokuapp.com/auth/year/add"
        else:
            url = "https://musicbrain-backend.herokuapp.com/" + \
                payload.get('endpoint')+"/add"
        response = requests.request("POST", url, json=payload)
        response = json.loads(response.content)
        return(response)


@auth.route('/connect', methods=['GET'])
def callback():
    if session.get('username') != None:
        return render_template('callback.html', username=session.get('username')or"User")
    else:
        return redirect('/auth/')

@auth.route('/post-login',methods=['GET'])
def handle_post_login():
    if session.get('username') != None:
        return redirect('/feed')
    else:
        return redirect('/auth/')


@auth.route('/callback', methods=['GET'])
def token_callback():
    code = request.args.get('code')
    if code != None:
        response = requests.request('GET', os.environ.get(
            'token')+code+"&username="+session['username'])
        response = json.loads(response.content)
        if response['status'] == 1:
            session['refresh_token'] = response['refresh_token']
            return redirect('/feed/')
        else:
            return ("<h3>Error communicating to spotify</h3>")
    else:
        return('<h2>Bad Request</h2><a href="/auth/">Click here to go back</a>'), 400


@auth.route('/logout')
def user_logout():
    session.clear()
    return redirect('/auth/')
