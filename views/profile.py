from flask import Flask, Blueprint, render_template, session, request, redirect
import requests
import json
from datetime import date,timedelta

profile = Blueprint('profile', __name__, template_folder="templates",
                    static_folder="static", url_prefix='/profile')


@profile.route('/<username>')
def prepare_profile(username):
    if session.get('username') != None:
        url = "https://musicbrain-backend.herokuapp.com/posts/profile?username="+username
        response = requests.request("GET", url)
        response = json.loads(response.content)

        url = "http://musicbrain-backend.herokuapp.com/friends/following?username="+username
        friends_response = requests.request("GET", url)
        friends_response = json.loads(friends_response.content)

        url = "http://musicbrain-backend.herokuapp.com/friends/count?username="+username
        count_response = requests.request("GET", url)
        count_response = json.loads(count_response.content)
        is_friend = False
        url = "http://musicbrain-backend.herokuapp.com/friends/requested?username=" + \
            session['username']
        requested_response = requests.request("GET", url)
        requested_response = json.loads(requested_response.content)
        session['requested'] = requested_response['friends']
        url = "http://musicbrain-backend.herokuapp.com/friends/following?username=" + \
            session['username']
        friends_response = requests.request("GET", url)
        friends_response = json.loads(friends_response.content)
        session['friends'] = friends_response['friends']
        print(session['requested'])
        is_requested = False
        #session['friends']
        is_user=False
        if session['username']!=username:
            is_user=True

        if username in session['friends']:
            is_friend = True
        if username in session ['requested']:
            is_requested = True

        if response['status'] and friends_response['status']:
            return render_template('user-profile.html', username=username,
                                   posts=response['posts'],
                                   user=session['username'],
                                   is_user=is_user,
                                   is_friend=is_friend,
                                   is_requested=is_requested,
                                   friends=friends_response['friends'],
                                   playlist_id=session['collab_reco'],
                                   requested=session['requested'],
                                   followers_count=count_response.get(
                                       'followers')or'NA',
                                   following_count=count_response.get('following')or'NA')
        else:
            return('<h2>Profile of requested user doesn\'t exist.</h2><a href="/feed">Go back to home</a>')
    else:
        return redirect('/auth/')


@profile.route('/<username>/<follow_type>')
def prepare_follow_list(username,follow_type):
    if session.get('username') != None:
        url = "https://musicbrain-backend.herokuapp.com/posts/profile?username="+username
        response = requests.request("GET", url)
        response = json.loads(response.content)

        url = "http://musicbrain-backend.herokuapp.com/friends/"+follow_type+"?username="+username
        friends_response = requests.request("GET", url)
        friends_response = json.loads(friends_response.content)
        friends=[]
        j=0
        is_user=False
        if session['username']==username:
            is_user=True
        url = "http://musicbrain-backend.herokuapp.com/friends/requested?username=" + \
            session['username']
        requested_response = requests.request("GET", url)
        requested_response = json.loads(requested_response.content)
        session['requested'] = requested_response['friends']
        url = "http://musicbrain-backend.herokuapp.com/friends/following?username=" + \
            session['username']
        friends_sessionresponse = requests.request("GET", url)
        friends_sessionresponse = json.loads(friends_sessionresponse.content)
        session['friends'] = friends_sessionresponse['friends']
        for friend in friends_response['friends']:
            friends.append({})
            friends[j]['friend']=friend
            if friend in session['friends']:
                friends[j]['is_friend'] = True
            else:
                friends[j]['is_friend'] = False
            if friend in session['requested']:
                friends[j]['is_requested'] = True
            else:
                friends[j]['is_requested'] = False
            j+=1
        is_followers=False
        if follow_type=="followers":
            is_followers=True
        if response['status'] and friends_response['status']:
            return render_template('follow-view.html', username=username,requested=session['requested'],
                                   user=session['username'],
                                   is_user=is_user,
                                   is_followers=is_followers,
                                   posts=response['posts'], friends=friends)
        else:
            return('<h2>Profile of requested user doesn\'t exist.</h2><a href="/feed">Go back to home</a>')
    else:
        return redirect('/auth/')

@profile.route('/request-follow', methods=['POST','DELETE'])
def request_follow():
    if request.method == 'POST' or request.method == 'DELETE':
        payload = request.get_json()
        if request.method == 'DELETE':
            if payload['type'] == "Remove":
                payload['friend'] = session['username']
            if payload['type'] == "Unfollow":
                payload['username'] = session['username']
        else:    
            payload['username'] = session['username']
        print(payload)
        if payload.get('endpoint')=="new":
            session['requested'].append(payload.get('friend'))
        if payload.get('endpoint')=="accept":
            session['requested'].remove(payload.get('friend'))
            print("Accepted")
        url = "https://musicbrain-backend.herokuapp.com/friends/"+payload.get('endpoint')
        if request.method == 'DELETE':
            response = requests.request("DELETE", url, json=payload)
        else:
            response = requests.request("POST", url, json=payload)
        response = json.loads(response.content)
        print(response)
    return(response)

@profile.route('/trends')
def show_trends():
    if session.get('username') != None:
        url = "https://musicbrain-backend.herokuapp.com/trends/mood"
        payload = {}
        payload['username']=session.get('username')
        payload['day']=7
        payload['recommendation']=0
        response = requests.request("POST", url, json=payload)
        response = json.loads(response.content)     #we get 7,2,10 and 30
        url_top10 = "https://musicbrain-backend.herokuapp.com/trends/top10?username="+session['username']
        top10 = requests.request("GET", url_top10)
        top10 = json.loads(top10.content)
        top10_Global_songCount = top10['top10_Global_songCount']
        top10_Global_songName = top10['top10_Global_songName']
        top10_myFriends_songName = top10['top10_myFriends_songName']
        top10_myFriends_songCount = top10['top10_myFriends_songCount']
        url_weekAct = "https://musicbrain-backend.herokuapp.com/trends/weeklyAct?username="+session['username']
        activityIn_7days = requests.request("GET", url_weekAct)
        activityIn_7days = json.loads(activityIn_7days.content)
        url = "http://musicbrain-backend.herokuapp.com/friends/requested?username=" + \
            session['username']
        requested_response = requests.request("GET", url)
        requested_response = json.loads(requested_response.content)
        session['requested'] = requested_response['friends']

        if response['status'] and top10['status'] and activityIn_7days['status']:
            return render_template('trends.html',activity_inPast7Days=activityIn_7days['countFor7Days'], username=session.get('username'),
                top10_Global_songCount=top10_Global_songCount,top10_Global_songName=top10_Global_songName,requested=session['requested'],
                days_seven=response['songs'][0],days_two=response['songs'][1],days_ten=response['songs'][2],days_thirty=response['songs'][3],
                top10_myFriends_songCount=top10_myFriends_songCount,top10_myFriends_songName=top10_myFriends_songName)
        else:
            return('<h2>Creation of Trends page failed.</h2><a href="/feed">Try again</a>')
    else:
        return redirect('/auth/')


@profile.route('/generate', methods=['POST'])
def update_trends():
    if session.get('username') != None:
        print("click")
        payload = request.get_json()
        payload['token']=session['token']
        payload['username']=session['username']
        print(payload)
        url = "http://musicbrain-backend.herokuapp.com/trends/colab_reco"
        response = requests.request("POST", url, json=payload)
        response = json.loads(response.content)
        session['collab_reco']=response['playlist']
        return (response)
    else:
        return redirect('/auth/')