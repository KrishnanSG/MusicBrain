# MusicBrain

**MusicBrain** is an application which provides both content based and collaborative based music recommendation. The application provides tailor made recommendations by thoroughly analyzing and understanding the user’s listening patterns. Patterns are observed using our custom built machine learning algorithms to create curated playlist for the user.

On creation of an account basic data is collected from the user and rest of the data pertaining to music listening history are fetched from the users spotify account. Apart from content based recommendation the application encourages users to socialize with other users through the socialize feature.

Users can expand their friends circle by following other users on the application. With this amazing network of music lovers formed, musicbrain recommends songs to users based on what their friends and other users with similar interests listen to.

Also users can share their opinion about a song as a post on the application. These posts will appear on the music feed of their friends. Users can listen, like and comment on these posts. The application also displays a Trends screen - an overview of the songs that are listened to by many users and songs that are trending in a particular geographic region.
